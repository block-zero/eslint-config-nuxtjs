module.exports = {
	root: true,
	extends: ['plugin:vue/recommended', '@nuxtjs', '@block-zero/eslint-config'],
	rules: {
		'vue/html-closing-bracket-newline': 'off',
		'vue/html-indent': 'off',
		'vue/html-self-closing': 'off',
		'vue/singleline-html-element-content-newline': 'off',
		'vue/max-attributes-per-line': 'off',
		'vue/multiline-html-element-content-newline': 'off',
	},
};

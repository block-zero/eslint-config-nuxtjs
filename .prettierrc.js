module.exports = {
	arrowParens: "avoid",
	bracketSpacing: true,
	endOfLine: "auto",
	printWidth: 80,
	quoteProps: "as-needed",
	trailingComma: "es5",
	tabs: true,
	tabWidth: 1,
	semi: true,
	singleQuote: true,
};

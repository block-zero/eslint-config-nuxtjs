# Block Zero ESLint default Configuration

## Usage

```bash
npm install -D eslint eslint-plugin-import eslint-plugin-vue @block-zero/eslint-config-nuxtjs
```

Then, add an ESLint config in the repo, for example, a`.eslintrc.json` with:
```json
{
	"extends": "@block-zero/eslint-config-nuxtjs",
};

```

This is the base setup, the project you use it in can overwrite and add things
as needed.
